package com.example.task04;

public class Line {
    private Point _p1, _p2;

    public Line(Point p1, Point p2) {
        if (p1 == null || p2 == null) {
            _p1 = new Point(0, 0);
            _p2 = new Point(0, 0);
        }
        _p1 = p1;
        _p2 = p2;
    }

    public boolean isCollinearLine(Point p) {
        if (p == null)
            return false;
        double source_length = _p1.distance(_p2);
        double new_length = _p1.distance(p) + p.distance(_p2);
        return new_length == source_length;
    }


    public double getLineLength() {
        return _p1.distance(_p2);
    }

    public Point getP1() {
        return _p1;
    }

    public Point getP2() {
        return _p2;
    }

    public String toString() {
        String result = "[" + _p1.toString() + ";" + _p2.toString() + "]";
        return result;
    }
}
