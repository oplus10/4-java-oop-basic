package com.example.task03;

public class ComplexNumber {
    private int realPart;
    private int imaginaryPart;

    public ComplexNumber(int realPart, int imaginaryPart) {
        this.realPart = realPart;
        this.imaginaryPart = imaginaryPart;
    }

    public ComplexNumber add(ComplexNumber other) {
        if (other == null)
            return new ComplexNumber(0,0);
        int newRealPart = this.realPart + other.realPart;
        int newImaginaryPart = this.imaginaryPart + other.imaginaryPart;
        return new ComplexNumber(newRealPart, newImaginaryPart);
    }

    // Метод для умножения комплексных чисел
    public ComplexNumber multiply(ComplexNumber other) {
        if (other == null)
            return new ComplexNumber(0, 0);
        int newRealPart = this.realPart * other.realPart - this.imaginaryPart * other.imaginaryPart;
        int newImaginaryPart = this.realPart * other.imaginaryPart + this.imaginaryPart * other.realPart;
        return new ComplexNumber(newRealPart, newImaginaryPart);
    }

    // Переопределяем метод toString для красивого вывода
    @Override
    public String toString() {
        return "(" + realPart + " + " + imaginaryPart + "i)";
    }
}
