package com.example.task03;

public class Task03Main {
    public static void main(String[] args) {
        ComplexNumber number1 = new ComplexNumber(1, 3),
                number2 = new ComplexNumber(3, 1),
                result;

        System.out.println("Number1 = " + number1.toString());
        System.out.println("Number2 = " + number2.toString());
        result = number1.add(number2);
        System.out.println("number1 + number2 = " + result.toString());
        result = number1.multiply(number2);
        System.out.println("number1 * number2 = " + result.toString());
    }
}
