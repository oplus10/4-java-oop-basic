package com.example.task01;

/**
 * Класс точки на плоскости
 */
public class Point {
    int x, y;

    Point(int init_x, int init_y) {
        this.x = init_x;
        this.y = init_y;
    }

    Point() {
        this.x = 0;
        this.y = 0;
    }

    void flip() {
        int temp = this.x;
        this.x = this.y * (-1);
        this.y = temp * (-1);
    }

    public String toString() {
        String retValue = "(";
        retValue += Integer.toString(this.x) + ",";
        retValue += Integer.toString(this.y) + ")";

        return retValue;
    }

    double distance(Point point) {
        if (point == null)
            return 0;

        double result = Math.sqrt((Math.pow(point.x - this.x, 2) + Math.pow(point.y - this.y, 2)));
        return result;
    }

    void print() {
        String pointToString = String.format("(%d, %d)", x, y);
        System.out.println(pointToString);
    }
}
