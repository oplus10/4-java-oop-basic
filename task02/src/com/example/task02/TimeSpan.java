package com.example.task02;

public class TimeSpan {
    private int _seconds, _minuts, _hours;

    TimeSpan(int hours, int minuts, int seconds) {
        if (hours < 0 || minuts < 0 || seconds < 0) {
            hours = 0;
            minuts = 0;
            seconds = 0;
        }
        if (minuts >= 60 || seconds >= 60) {
            int tempSec = (minuts * 60) + seconds;
            _minuts = tempSec / 60;
            _seconds = tempSec % 60;
            _hours = hours;
            if (_minuts >= 60) {
                _hours = hours + (_minuts / 60);
                _minuts = _minuts % 60;
            }
        } else {
            _hours = hours;
            _minuts = minuts;
            _seconds = seconds;
        }
    }

    private boolean initFromSeconds(long totalSeconds) {
        if (totalSeconds < 0)
            return false;
        long minuts = totalSeconds / 60;
        if (minuts >= 60) {
            this._hours = (int) minuts / 60;
            this._minuts = (int) minuts % 60;
        }
        this._seconds = (int) totalSeconds % 60;

        return true;
    }

    public int getSeconds() {
        return this._seconds;
    }

    public int getMinuts() {
        return this._minuts;
    }

    public int getHours() {
        return this._hours;
    }

    public boolean setMinuts(int minuts) {
        if (minuts < 0)
            return false;
        if (minuts >= 60) {
            TimeSpan tempTs = new TimeSpan(_hours, minuts, _seconds);
            this._hours = tempTs._hours;
            this._minuts = tempTs._minuts;
            this._seconds = tempTs._seconds;
        } else {
            this._minuts = minuts;
        }
        return true;
    }

    public boolean setSeconds(int seconds) {
        if (seconds < 0)
            return false;
        if (seconds >= 60) {
            TimeSpan tempTs = new TimeSpan(this._hours, this._minuts, seconds);
            this._hours = tempTs._hours;
            this._minuts = tempTs._minuts;
            this._seconds = tempTs._seconds;
        } else {
            this._seconds = seconds;
        }
        return true;
    }

    public boolean setHours(int hours) {
        if (hours < 0)
            return false;
        this._hours = hours;
        return true;
    }

    public void add(TimeSpan time) {
        if (time == null)
            return;
        int hours = this._hours + time._hours;
        int minuts = this._minuts + time._minuts;
        int seconds = this._seconds + time._seconds;

        if (minuts >= 60 || seconds >= 60) {
            long totalSeconds = hours * 3600 + minuts * 60 + seconds;
            initFromSeconds(totalSeconds);
        }
        return;
    }

    public void subtract(TimeSpan time) {
        if (time == null)
            return;
        TimeSpan tempTs = new TimeSpan(this._hours, this._minuts, this._seconds);
        tempTs._hours -= time._hours;
        tempTs._minuts -= time._minuts;
        tempTs._seconds -= time._seconds;
        long sourceTotalSeconds = this._hours * 3600 + this._minuts * 60 + this._seconds,
                timeTotalSeconds = time._hours * 3600 + time._minuts * 60 + time._seconds;

        if (sourceTotalSeconds < timeTotalSeconds)
            return;
        sourceTotalSeconds -= timeTotalSeconds;
        initFromSeconds(sourceTotalSeconds);

        return;
    }

    public String toString() {
        String result = Integer.toString(this._hours) + ":" + Integer.toString(this._minuts) + ":" + Integer.toString(this._seconds);
        return result;
    }
}

