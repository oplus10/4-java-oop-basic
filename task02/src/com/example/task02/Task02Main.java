package com.example.task02;

import java.sql.Time;

public class Task02Main {
    public static void main(String[] args) {
        TimeSpan time = new TimeSpan(12, 345, 432425),
                time2 = new TimeSpan(3, 12, 4995);
        System.out.println("Time1 = " + time.toString());
        System.out.println("Time2 = " + time2.toString());
        time.add(time2);
        System.out.println("Total time = " + time.toString());
    }
}
