package com.example.task05;

/**
 * Точка в двумерном пространстве
 */
public class Point {
    private double _x, _y;
    /**
     * Конструктор, инициализирующий координаты точки
     *
     * @param x координата по оси абсцисс
     * @param y координата по оси ординат
     */
    public Point(double x, double y) {
        this._x = x;
        this._y = y;
    }

    /**
     * Возвращает координату точки по оси абсцисс
     *
     * @return координату точки по оси X
     */
    public double getX() {
        return this._x;
    }

    /**
     * Возвращает координату точки по оси ординат
     *
     * @return координату точки по оси Y
     */
    public double getY() {
        return this._y;
    }

    /**
     * Подсчитывает расстояние от текущей точки до точки, переданной в качестве параметра
     *
     * @param point вторая точка отрезка
     * @return расстояние от текущей точки до переданной
     */
    public double getLength(Point point) {
        if (point == null)
            return 0;

        double result = Math.sqrt((Math.pow(point._x - this._x, 2) + Math.pow(point._y - this._y, 2)));
        return result;
    }

}
